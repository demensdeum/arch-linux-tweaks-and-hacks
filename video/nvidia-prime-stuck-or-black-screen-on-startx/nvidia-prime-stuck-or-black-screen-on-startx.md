# Nvidia Prime Stuck Or Black Screen
Add nvidia-drm.modeset=1 to kernel params.

## Example for grub.cfg
### Before
linux	/vmlinuz-linux root=UUID=66a4bfd8-015c-4c59-b10e-d6d79e7f661d rw  loglevel=3 quiet

### After
linux	/vmlinuz-linux root=UUID=66a4bfd8-015c-4c59-b10e-d6d79e7f661d rw  loglevel=3 quiet nvidia-drm.modeset=1